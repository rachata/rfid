create database dbrf
	default char set utf8
    default collate utf8_general_ci
    
use dbrf

create table login(
	id int primary key auto_increment,
    username varchar(100),
    password varchar(100),
    display_name varchar(100)
)

create table card(
	id int primary key auto_increment,
    card_id varchar(256), 
    card_status boolean default false
)


create table card_user(
	id int primary key auto_increment,
    card_id int ,
    sid varchar(256),
    sfname varchar(256),
    slname varchar(256),
    foreign key (card_id) references card(id)
)

create table log(
	id int primary key auto_increment,
    id_card_user int,
    log_status boolean,
    log_dt timestamp default CURRENT_TIMESTAMP,
    foreign key (id_card_user) references card_user(id)
)